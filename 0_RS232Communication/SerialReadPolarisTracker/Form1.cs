﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO; //for reading files
using System.IO.Ports; //for serial port
using System.Windows.Forms.DataVisualization.Charting; //for chart plotting




namespace SerialReadPolarisTracker
{
    public partial class Form1 : Form
    {

        SerialPort PolarisCOMPort = new SerialPort();
   

        bool trackingMode = false;
        bool diagnosticMode = false;
        bool initialised = false;
        string lastCommand = null;

        

        string MarkerInfoStr = "";
        byte[] NoOfMarkers = { 0x00, 0x00, 0x00, 0x00 };
        List<decimal> MarkerCoords = new List<decimal>();

        string portHandle = "01";
        



        public Form1()
        {
            
            PolarisCOMPort.PortName = "COM5";
            PolarisCOMPort.BaudRate = 9600;
            PolarisCOMPort.DtrEnable = true;
            PolarisCOMPort.RtsEnable = true;

            if (PolarisCOMPort.IsOpen)
            {
                PolarisCOMPort.Close();
            }

            PolarisCOMPort.DataReceived += new SerialDataReceivedEventHandler(PolarisCOMPort_DataReceived);

            

            InitializeComponent();

            updateDisplayTimer.Enabled = true;
            updateDisplayTimer.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (PolarisCOMPort.IsOpen)
            {
                PolarisCOMPort.Close();
            }

            try
            {
                PolarisCOMPort.Open();
                //button1.Enabled = false;
                //richTextBox1.Enabled = true;
            }
            catch
            {

            }

            updateButtonStates();


        }

        private void PolarisCOMPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            //string indata = sp.ReadExisting();

  
            //updateRichTextBox(indata);


            int bytes = sp.BytesToRead;
            byte[] buffer = new byte[bytes];
            sp.Read(buffer, 0, bytes);

           
            string indata = System.Text.Encoding.ASCII.GetString(buffer);
            updateRichTextBox(indata);
            // int bytesToRead = sp.BytesToRead;

            //string indata = bytesToRead.ToString();
            if (indata == "RESETBE6F\r")
            {
                updateRichTextBox("PolarisOpticalTracker First Time Power On\r");
            }
            // string indata = sp.ReadExisting();
            if (indata == "OKAYA896\r")
            {
                if (lastCommand == "INIT")
                {
                    initialised = true;
                    
                }
                else if (lastCommand == "TSTOP")
                {
                    trackingMode = false;
                }

                else if (lastCommand == "TSTART")
                {
                    trackingMode = true;
                }
                else if (lastCommand == "DSTART")
                {
                    diagnosticMode = true;
                    
                }
                else if (lastCommand == "DSTOP")
                {
                    diagnosticMode = false;
                }
               
            }

            //if (lastCommand == "PHRQ")
            //{
            //    portHandle = indata.Substring(0,2);
            //    Console.WriteLine(portHandle);

            //}

            if (lastCommand == "3D 015")
            {
   
               MarkerInfoStr += System.Text.Encoding.ASCII.GetString(buffer);
               lastCommand = "3D 015";

                if (MarkerInfoStr.Contains("\r"))
                {
                    string[] individMarkerInfo = MarkerInfoStr.Split('\n');
                    
                    updateRichTextBox("\nNo Of Markers: " + individMarkerInfo[0]);
                    Invoke(new Action(() => {
                        while (chart1.Series.Count > 0) { chart1.Series.RemoveAt(0); }
                        while (chart2.Series.Count > 0) { chart2.Series.RemoveAt(0); }
                        while (chart3.Series.Count > 0) { chart3.Series.RemoveAt(0); }

                    }));

                    for (int i = 1; i < individMarkerInfo.Length; i++)
                    {
                        // individMarkerInfo[i] = System.Text.RegularExpressions.Regex.Unescape(individMarkerInfo[i]);
                        // individMarkerInfo[i] = individMarkerInfo[i].Replace("\\", "\n");
                        individMarkerInfo[i] = individMarkerInfo[i].Replace("+", "\t+");
                        individMarkerInfo[i] = individMarkerInfo[i].Replace("-", "\t-");
                        // individMarkerInfo[i].TrimStart("\\").TrimEnd('\\');
                        updateRichTextBox("\nMarker" + i.ToString() + "XYZ: " + individMarkerInfo[i]);

                        string[] XYZmarkerinfo= individMarkerInfo[i].Split('\t');
                        try
                        {
                            decimal X = Convert.ToDecimal(XYZmarkerinfo[1]) / (decimal)1000.00;
                            decimal Y = Convert.ToDecimal(XYZmarkerinfo[2]) / (decimal)1000.00;
                            decimal Z = Convert.ToDecimal(XYZmarkerinfo[3]) / (decimal)1000.00;


                            MarkerCoords.Add(X);
                            MarkerCoords.Add(Y);
                            MarkerCoords.Add(Z);

                            Series markerSeries;

                            Invoke(new Action(() =>
                            {

                                markerSeries = this.chart1.Series.Add("Marker" + i.ToString());
                                markerSeries.ChartType = SeriesChartType.Point;
                                markerSeries.Points.AddXY((double)X, (double)Y);
                                this.chart1.ChartAreas[0].AxisX.Title = "X(mm)";
                                this.chart1.ChartAreas[0].AxisY.Title = "Y (mm)";

                                this.chart1.ChartAreas[0].AxisX.Maximum = 1500.00;
                                this.chart1.ChartAreas[0].AxisX.Minimum = -1500.00;
                                this.chart1.ChartAreas[0].AxisY.Maximum = 1500.00;
                                this.chart1.ChartAreas[0].AxisY.Minimum = -1500.00;
                                // this.chart1.ChartAreas[0].AxisY.Title = "Y (mm)";

                                
                                markerSeries = this.chart2.Series.Add("Marker" + i.ToString());
                                markerSeries.ChartType = SeriesChartType.Point;
                                markerSeries.Points.AddXY((double)Z, (double)Y);
                                this.chart2.ChartAreas[0].AxisX.Title = "Z (mm)";
                                this.chart2.ChartAreas[0].AxisY.Title = "Y (mm)";

                                this.chart2.ChartAreas[0].AxisX.Maximum = 0.00;
                                this.chart2.ChartAreas[0].AxisX.Minimum = -15000.00;
                                this.chart2.ChartAreas[0].AxisY.Maximum = 1500.00;
                                this.chart2.ChartAreas[0].AxisY.Minimum = -1500.00;

                                markerSeries = this.chart3.Series.Add("Marker" + i.ToString());
                                markerSeries.ChartType = SeriesChartType.Point;
                                markerSeries.Points.AddXY((double)X, (double)Z);
                                this.chart3.ChartAreas[0].AxisX.Title = "X (mm)";
                                this.chart3.ChartAreas[0].AxisY.Title = "Z (mm)";

                                this.chart3.ChartAreas[0].AxisX.Maximum = 1500.00;
                                this.chart3.ChartAreas[0].AxisX.Minimum = -1500.00;
                                this.chart3.ChartAreas[0].AxisY.Maximum = 0.00;
                                this.chart3.ChartAreas[0].AxisY.Minimum = -15000.00;

                            }));

                        }
                        catch
                        {

                        }
                       

                    }

                    MarkerCoords.Clear();

                    MarkerInfoStr = null;
                    lastCommand = null;
                }

            }




            // updateButtonStates();
        }


            private void button5_Click(object sender, EventArgs e)
        {
            lastCommand = "INIT";
            PolarisCOMPort.Write(lastCommand+" \r");
            updateRichTextBox(lastCommand+"\t");
            updateButtonStates();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lastCommand = "BEEP 3";
            PolarisCOMPort.Write(lastCommand+"\r");
            updateRichTextBox(lastCommand + "\t");
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            if (trackingMode)
            {
                lastCommand = "TSTOP";
            }
            else
            {
                lastCommand = "TSTART";
            }

            PolarisCOMPort.Write(lastCommand+" \r");
            updateRichTextBox(lastCommand + "\t");
        }

        private void updateRichTextBox(string textToAppend)   
        {
            Invoke(new Action(() => {richTextBox1.AppendText(textToAppend); richTextBox1.ScrollToCaret(); }));
        }



        private void updateDisplayTimer_Tick(object sender, EventArgs e)
        {
            updateDisplayTimer.Stop();


            try
            {
                if (PolarisCOMPort.IsOpen && diagnosticMode == true)
                {
                    lastCommand = "3D 015";
                    PolarisCOMPort.Write(lastCommand + "\r");
                    updateRichTextBox(lastCommand + "\t");
                }
            }
            catch
            {
            }


            updateButtonStates();

    
           
            updateDisplayTimer.Start();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
            {
                lastCommand = "0001";
            }
            else if (radioButton2.Checked)
            {
                lastCommand = "0002";
            }
            else if (radioButton3.Checked)
            {
                lastCommand = "0008";
            }
            else if (radioButton4.Checked)
            {
                lastCommand = "0801";
            }
            else if (radioButton5.Checked)
            {
                lastCommand = "1000";
            }

            PolarisCOMPort.Write("TX " + lastCommand + "\r");
            updateRichTextBox("TX " + lastCommand + "\t");
        }

        private void updateButtonStates()
        {
            if (PolarisCOMPort.IsOpen)
            {
                richTextBox1.Enabled = true;
                button5.Enabled = true;
                button4.Enabled = true;
                button2.Enabled = true;
                button1.Enabled = false;
                button6.Enabled = true;
            }
            else
            {
                richTextBox1.Enabled = false;
                button5.Enabled = false;
                button1.Enabled = true;


                button4.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button6.Enabled = false;

                radioButton1.Enabled = false;
                radioButton2.Enabled = false;
                radioButton3.Enabled = false;
                radioButton4.Enabled = false;
                radioButton5.Enabled = false;
            }

            if (initialised)
            {
                button4.Enabled = true;
                button2.Enabled = true;
            }
            else
            {
                //button4.Enabled = false;
                //button2.Enabled = false;
            }

            if (trackingMode)
            {
                button2.Text = "TSTOP";
                button3.Enabled = true;
                radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton3.Enabled = true;
                radioButton4.Enabled = true;
                radioButton5.Enabled = true;
            }
            else
            {
                button2.Text = "TSTART";
                button3.Enabled = false;
                radioButton1.Enabled = false;
                radioButton2.Enabled = false;
                radioButton3.Enabled = false;
                radioButton4.Enabled = false;
                radioButton5.Enabled = false;
            }

            if (diagnosticMode)
            {
                button6.Text = "DSTOP";
            }
            else
            {
                button6.Text = "DSTART";
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (diagnosticMode)
            {
                lastCommand = "DSTOP";
            }
            else
            {
                lastCommand = "DSTART";
            }


            PolarisCOMPort.Write(lastCommand + " \r");
            updateRichTextBox(lastCommand + "\t");
            updateButtonStates();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            lastCommand = "3D 015";
            PolarisCOMPort.Write(lastCommand + "\r");
            updateRichTextBox(lastCommand + "\t");
            updateButtonStates();
        }

        private void plotMarker(string coords)
        {
            string[] multiMarkerCoords = coords.Split('\n');

            string marker1 = multiMarkerCoords[1];

            updateRichTextBox("Marker1:Coords = " + marker1);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            updateRichTextBox("MarkerInfo: " + MarkerInfoStr);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }



        private void phrq_button_Click(object sender, EventArgs e)
        {
            lastCommand = "PHRQ";
            string hardware_device = "********";
            string system_type = "*";
            string tool_type = "1";
            string port_numb = "**";
            PolarisCOMPort.Write(lastCommand + " " + hardware_device + system_type + tool_type + port_numb + "**" + "\r");
            updateRichTextBox(lastCommand + "\t");
            updateButtonStates();
        }

        private void loadRom_Click(object sender, EventArgs e)
        {
            byte[] romFile = File.ReadAllBytes("../../AccurateStrykerBaseArray2.rom");
            int lengthOfFile = romFile.Length;
            
            int modNo = lengthOfFile % 64;

            //byte[,] romFilePadded = new byte[(64 - modNo), lengthOfFile];
            byte[] romFilePadded = new byte[lengthOfFile+(64 - modNo)];

            if (modNo != 0)
            {
               
                for (int i = 0; i < lengthOfFile; i++)
                {
                    //romFilePadded[0, i] = romFile[i];
                    romFilePadded[i] = romFile[i];
                }
                lengthOfFile = lengthOfFile + (64 - modNo);

                Console.WriteLine(romFilePadded.ToString());
            }

            //write file to polarisDevice
            for(int i=0; i < lengthOfFile/64; i++)
            {
                //increment i by 64bit chunks
                int startAddrInt = i * 64;
                string startAddr = startAddrInt.ToString("X4");
                Console.WriteLine("int:" + i.ToString() + " dec:" + startAddrInt.ToString()+" hex:" + startAddr);

                string writeData = null;
                for (int j =0; j < 64; j++)
                {
                    writeData += (romFilePadded[startAddrInt + j]).ToString("X2");
                }
                
                lastCommand = "PVWR";
                PolarisCOMPort.Write(lastCommand + " 01" + startAddr + writeData + "\r");
                //updateRichTextBox(lastCommand + "\t");
                
                for (int delay=0; delay < 100000000; delay++)
                {

                }

                Console.WriteLine("AppendedString: " + writeData + ", String Length: " + writeData.Length);

            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            lastCommand = "PINIT";
            PolarisCOMPort.Write(lastCommand + " 01" + "\r");
            updateRichTextBox(lastCommand + "\t");

        }

        private void button10_Click(object sender, EventArgs e)
        {
            lastCommand = "PENA";
            PolarisCOMPort.Write(lastCommand + " 01" + "D" + "\r");
            updateRichTextBox(lastCommand + "\t");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            lastCommand = "PHSR";
            PolarisCOMPort.Write(lastCommand + " \r");
            updateRichTextBox(lastCommand + "\t");
            updateButtonStates();
        }

        private void capture_btn_Click(object sender, EventArgs e)
        {

        }
    }
}
