﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Stateless;
using Stateless.Graph;

namespace SerialCommunicatorForPolarisP4
{
    class P4Initi
    {
        //public enum State { standby, INIT, PHSR, PHRQ, PVWR, PINIT, PENA, passed, failed }
        //public enum Trigger { begin, getPortHandles, requestPortHandles, loadToolDefFile, initPortHandles, enablePortHandles, pass, fail }
        //private readonly StateMachine<State, Trigger> _machine;

        //public P4Initi(State state)
        //{
        //    _machine = new StateMachine<State, Trigger>(state);

        //    _machine.Configure(State.standby)
        //        .Permit(Trigger.begin, State.INIT);

        //    _machine.Configure(State.INIT)
        //        .Permit(Trigger.getPortHandles, State.PHSR)
        //        .Permit(Trigger.fail, State.failed);

        //    _machine.Configure(State.PHSR)
        //        .Permit(Trigger.requestPortHandles, State.PHRQ)
        //        .Permit(Trigger.fail, State.failed);

        //    _machine.Configure(State.PHRQ)
        //        .Permit(Trigger.loadToolDefFile, State.PVWR)
        //        .Permit(Trigger.fail, State.failed);

        //    _machine.Configure(State.PVWR)
        //        .Permit(Trigger.initPortHandles, State.PINIT)
        //        .Permit(Trigger.fail, State.failed);

        //    _machine.Configure(State.PINIT)
        //        .Permit(Trigger.enablePortHandles, State.PENA)
        //        .Permit(Trigger.fail, State.failed);

        //}

        public enum State { standby, INIT, PHSR, PHRQ, passed, failed }

        public enum Trigger { begin, getPortHandles, requestPortHandles, pass, fail }
        private readonly StateMachine<State, Trigger> _machine;

      //  private readonly StateMachine<State, Trigger>.TriggerWithParameters<string> _beginTrigger;

        public P4Initi()
        {
            //Instantiate a new state machine in the standby state
            _machine = new StateMachine<State, Trigger>(State.standby);


            ////Instantiate a new trigger with a parameter
            //_beginTrigger = _machine.SetTriggerParameters<string>(Trigger.begin);


            //configure the standby state
            _machine.Configure(State.standby)
                .Permit(Trigger.begin, State.INIT);

            //configure the INIT state
            _machine.Configure(State.INIT)
                .Permit(Trigger.getPortHandles, State.PHSR)
                .Permit(Trigger.fail, State.failed);

            //configure the port handle get state
            _machine.Configure(State.PHSR)
                .Permit(Trigger.requestPortHandles, State.PHRQ)
                .Permit(Trigger.fail, State.failed);

            //configure the port handle request state
            _machine.Configure(State.PHRQ)
                .Permit(Trigger.pass, State.passed)
                .Permit(Trigger.fail, State.failed);

            _machine.Configure(State.passed)
                .Permit(Trigger.begin, State.standby)
                .Permit(Trigger.fail, State.failed);

            _machine.Configure(State.failed)
                .Permit(Trigger.begin, State.standby);
        }

        //trigger functions

        public void begin()
        {
            _machine.Fire(Trigger.begin);
        }

        public void getPortHandles()
        {
            _machine.Fire(Trigger.getPortHandles);
        }

        public void requestPortHandles()
        {
            _machine.Fire(Trigger.requestPortHandles);
        }

        public void pass()
        {
            _machine.Fire(Trigger.pass);
        }

        public void fail()
        {
            _machine.Fire(Trigger.fail);
        }

        void Onbegin()
        {
            Console.WriteLine("beginning...writing INIT");

        }

        void OngetPortHandles()
        {
            Console.WriteLine(" gettingPortHandles...writing PHSR");
        }

        void OnrequestPortHandles()
        {
            Console.WriteLine("requestingPortHandles...writing PHRQ");
        }

        void Onpass()
        {
            Console.WriteLine("passed first initialisation routine successfully!");
        }

        void Onfail()
        {
            Console.WriteLine("failed init :(");
        }

        public string ToDotGraph()
        {
            return UmlDotGraph.Format(_machine.GetInfo());
        }



    }
}
