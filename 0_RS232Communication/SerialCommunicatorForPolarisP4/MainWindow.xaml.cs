﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Forms;

using RTSerialCom; // for better and faster serial communication
using System.Text.RegularExpressions; // to use regex for smart serial port display
using Microsoft.Win32; //for registry key use
using System.IO.Ports; // for SerialPorts
using System.IO; //for reading files

namespace SerialCommunicatorForPolarisP4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //image variable to store addresses of icons
        BitmapImage connect_ico = new BitmapImage(new Uri(@"Resources/connect_ico.png", UriKind.Relative));
        BitmapImage disconnect_ico = new BitmapImage(new Uri(@"Resources/disconnect_ico.png", UriKind.Relative));

        string lastCommand = null;
        string lastAction = null;

        string currentStatusStr = null;

        //variables for statuses and modes
        bool status_sercon =   false;  //connected to serial port or not

        bool status_polpwr  =   false;  //whether the polaris has been powered on for the first time
        bool status_polcon  =   false;  //whether polaris is connected and communicating
        bool status_init    =   false;  //whether .rom file has been loaded and complete initialisation sequence is completed 

        bool status_phsr = false;
        bool status_phrq = false;
        bool status_pvwr = false; //writing .rom file with PVWR command
        bool status_pinit = false;
        bool status_pena    =   false;

        bool mode_diagnostic    =   false;  //needs to be true to track individual markers

        bool mode_recordingtools    =   false;  //when true all other user input functions should be disabled
        bool status_validrectool    =   false;  //must be true  to create a good tool definition file using 6D architect.

        bool mode_tracking  =   false;  //needs to be true to track tools

        bool status_running = false; //used to enable disable buttons when running

        bool status_udpcon = false; //whether udp port successfully opened and whether other end responds
        bool status_streaming =   false;  //whether marker/tool coords are being broadcast

       

        SerialClient rs232Serial;

        Timer initSequenceTimer = new Timer(); // to handle the sequence of processes required for the initialisation of the P4 device.
        Timer toolinitSequenceTimer = new Timer(); //to handle the tool initialisation


        string MarkerInfoStr = "";
        byte[] NoOfMarkers = { 0x00, 0x00, 0x00, 0x00 };
        List<decimal> MarkerCoords = new List<decimal>();

        List<List<float>> markersPos = new List<List<float>>(); 




        public MainWindow()
        {
            //add in code to find serial device

            initSequenceTimer.Tick += initSequenceTimer_Tick;
            toolinitSequenceTimer.Tick += toolinitSequenceTimer_Tick;

            initSequenceTimer.Interval = 500; //in milliseconds

            toolinitSequenceTimer.Interval = 500; //in milliseconds()

            //VID and PID numbers of the USB to serial converter cable can be found by going to Device Manager -> Under Ports (COM & LPT) select your device -> Details -> Then Select 'Hardware Ids' from the drop down list
            string VID = "0403";
            string PID = "6001";
           
            InitializeComponent();

            connectser_box.Items.Add(getUSBSerialPortNames(VID, PID));
            connectser_box.SelectedIndex = 0;

            if (connectser_box.SelectedItem.ToString().Contains("ERROR:"))
            {
                connectser_btn.IsEnabled = false;
            }
            else
            {
                connectser_btn.IsEnabled = true;
            }

            updateDisplay();
            
            

            //rs232Serial = new SerialClient("COM5", 9600);
            //rs232Serial.OnReceiving += new EventHandler<DataStreamEventArgs>(serialReceiveHandler);


        }

        private void initSequenceTimer_Tick(object sender, EventArgs e)
        {
            initSequenceTimer.Stop();

            if (status_init == false)
            {
                lastCommand = "INIT";
                rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " \r"));

                initSequenceTimer.Start();
            }
            else
            {
                if (status_phsr == false)
                {
                    lastCommand = "PHSR";
                    rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " \r"));

                    initSequenceTimer.Start();

                    //status_phsr = true;
                }
                else
                {
                    if (status_phrq == false)
                    {
                        lastCommand = "PHRQ";
                        string hardware_device = "********";
                        string system_type = "*";
                        string tool_type = "1";
                        string port_numb = "**";
                        //PolarisCOMPort.Write(lastCommand + " " + hardware_device + system_type + tool_type + port_numb + "**" + "\r");
                        //updateRichTextBox(lastCommand + "\t");
                        rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " " + hardware_device + system_type + tool_type + port_numb + "**" + "\r"));

                        initSequenceTimer.Start();
                    }

                    else
                    {
                        initSequenceTimer.Stop();
                    }

                }
            }

            
        }

        private void toolinitSequenceTimer_Tick(object sender, EventArgs e)
        {
            toolinitSequenceTimer.Stop();
            if (status_pvwr == true)
            {

                if (status_pinit == false)
                {
                    lastCommand = "PINIT";
                    rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " 01" + "\r"));

                    toolinitSequenceTimer.Start();
                }
                else
                {
                    if (status_pena == false)
                    {
                        lastCommand = "PENA";
                        rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " 01" + "D" + "\r"));

                        toolinitSequenceTimer.Start();
                    }
                    else
                    {
                        toolinitSequenceTimer.Stop();
                    }
                }

            }
        }

        private void serialReceiveHandler(object sender, DataStreamEventArgs e)
        {
            int currentLen = e.Response.Length;
            string indata = System.Text.Encoding.ASCII.GetString(e.Response, 0, currentLen);

            

            if (lastCommand == "BEEP 1")
            {
                status_polcon = true;
            }


            if (indata == "RESETBE6F\r")
            {
                status_polpwr = true;
                status_polcon = true;
                currentStatusStr = "status polpwr: " + status_polpwr.ToString();
            }
            else if (indata == "OKAYA896\r")
            {
                if (lastCommand == "INIT")
                {
                    status_init = true;
                    //initSequenceTimer.Start();


                }
                else if (lastCommand == "PVWR")
                {
                    status_pvwr = true;
                }

                else if (lastCommand == "PINIT")
                {
                    status_pinit = true;
                }

                else if (lastCommand == "PENA")
                {
                    status_pena = true;
                }

                else if (lastCommand == "TSTART")
                {
                    // status_init = true;
                    mode_tracking = true;
                }

                else if (lastCommand == "TSTOP")
                {
                    //status_init = false;
                    mode_tracking = false;
                }
                else if (lastCommand == "DSTART")
                {
                    // status_init = false;
                    mode_diagnostic = true;
                }
                else if (lastCommand == "DSTOP")
                {
                    //status_init = true;
                    mode_diagnostic = false;
                }

                currentStatusStr = lastCommand + " success";
            }

            else if (lastCommand == "3D 015")
            {

                    MarkerInfoStr += indata;
                    lastCommand = "3D 015";

                    if (MarkerInfoStr.Contains("\r"))
                    {
                        string[] individMarkerInfo = MarkerInfoStr.Split('\n');

                        for (int i = 1; i < individMarkerInfo.Length; i++)
                        {
                            individMarkerInfo[i] = individMarkerInfo[i].Replace("+", "\t+");
                            individMarkerInfo[i] = individMarkerInfo[i].Replace("-", "\t-");
                         

                            string[] XYZmarkerinfo = individMarkerInfo[i].Split('\t');
                            try
                            {
                                decimal X = Convert.ToDecimal(XYZmarkerinfo[1]) / (decimal)10000.00;
                                decimal Y = Convert.ToDecimal(XYZmarkerinfo[2]) / (decimal)10000.00;
                                decimal Z = Convert.ToDecimal(XYZmarkerinfo[3]) / (decimal)10000.00;


                                MarkerCoords.Add(X);
                                MarkerCoords.Add(Y);
                                MarkerCoords.Add(Z);


                            }
                            catch
                            {

                            }


                        }

                        MarkerCoords.Clear();

                        MarkerInfoStr = null;
                        lastCommand = null;
                    }
                }

                else if (indata.Contains("ERR")) //if error
                {
                    if (lastCommand == "PHSR")
                    {
                        status_init = false;
                        status_phsr = false;

                        // initSequenceTimer.Start();
                    }
                    else if (lastCommand == "PHRQ")
                    {
                        status_init = false;
                        status_phsr = false;
                        status_phrq = false;

                        //initSequenceTimer.Start();
                    }
                    else if (lastCommand == "PVWR")
                    {
                        status_pvwr = false;
                    }
                    else if (lastCommand == "PINIT")
                    {
                        status_pinit = false;
                    }

                    else if (lastCommand == "PENA")
                    {
                        status_pena = false;
                    }

                    currentStatusStr = lastCommand + " error";
                }
                else
                {
                    if (lastCommand == "PHSR")
                    {
                        status_phsr = true;
                        //initSequenceTimer.Start();

                    }
                    if (lastCommand == "PHRQ")
                    {

                        status_phrq = true;
                        //initSequenceTimer.Start();

                    }

                    currentStatusStr = lastCommand + " success";

                }

        updateDisplay();           
        }

        private void Connectser_btn_Click(object sender, RoutedEventArgs e)
        {
            if (status_sercon == false)
            {
               rs232Serial = new SerialClient(connectser_box.SelectedItem.ToString(), 9600);
               rs232Serial.OnReceiving += new EventHandler<DataStreamEventArgs>(serialReceiveHandler);
               status_sercon = rs232Serial.OpenConn(); // openconn returns true if open was sucessful
                if(status_sercon == false)
                {
                    status_sercon = rs232Serial.ResetConn();

                    if (status_sercon == false)
                    {
                        //could not connect message
                    }
                }
            }
            else // if serial connection is already open
            {
                rs232Serial.CloseConn();
                rs232Serial.OnReceiving -= new EventHandler<DataStreamEventArgs>(serialReceiveHandler);
                rs232Serial.Dispose();
                status_sercon = false;
            }

            currentStatusStr = "status sercon: " + status_sercon.ToString();

            updateDisplay();
        }

        private void Connectudp_btn_Click(object sender, RoutedEventArgs e)
        {
            
            status_udpcon = !status_udpcon;
            updateDisplay();
        }

        private void Init_btn_Click(object sender, RoutedEventArgs e)
        {
            status_init = false;
            status_phsr = false;
            status_phrq = false;

            initSequenceTimer.Start();

            //var initStateMach = new P4Initi();

            //initStateMach.begin();
            //initStateMach.getPortHandles();
            //initStateMach.requestPortHandles();

        }

        private void Loadtool_btn_Click(object sender, RoutedEventArgs e)
        {
            status_pvwr = false;
            status_pinit = false;
            status_pena = false;

            Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog();

            Nullable<bool> result = openFileDlg.ShowDialog();

            if(result == true)
            {
                loadtool_box.Items.Add(openFileDlg.FileName.ToString());
                loadtool_box.SelectedIndex = 0;

                byte[] romFile = File.ReadAllBytes(loadtool_box.SelectedItem.ToString());
                int lengthOfFile = romFile.Length;

                int modNo = lengthOfFile % 64;

                //byte[,] romFilePadded = new byte[(64 - modNo), lengthOfFile];
                byte[] romFilePadded = new byte[lengthOfFile + (64 - modNo)];

                if (modNo != 0)
                {

                    for (int i = 0; i < lengthOfFile; i++)
                    {
                        //romFilePadded[0, i] = romFile[i];
                        romFilePadded[i] = romFile[i];
                    }
                    lengthOfFile = lengthOfFile + (64 - modNo);

                    Console.WriteLine(romFilePadded.ToString());
                }

                //write file to polarisDevice
                for (int i = 0; i < lengthOfFile / 64; i++)
                {
                    //increment i by 64bit chunks
                    int startAddrInt = i * 64;
                    string startAddr = startAddrInt.ToString("X4");
                    Console.WriteLine("int:" + i.ToString() + " dec:" + startAddrInt.ToString() + " hex:" + startAddr);

                    string writeData = null;
                    for (int j = 0; j < 64; j++)
                    {
                        writeData += (romFilePadded[startAddrInt + j]).ToString("X2");
                    }

                   // status_pvwr = false;

                    lastCommand = "PVWR";
                    rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " 01" + startAddr + writeData + "\r"));
                    

                    for (int delay = 0; delay < 100000000; delay++)
                    {

                    }

                    //Console.WriteLine("AppendedString: " + writeData + ", String Length: " + writeData.Length);

                }
            }
            //status_pena = true;
            updateDisplay();

            toolinitSequenceTimer.Start();
        }

        private void Savetool_btn_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            if (saveFileDialog.ShowDialog() == true)
            {

            }
        }

        private void Tracktool_btn_Click(object sender, RoutedEventArgs e)
        {
            //mode_tracking = !mode_tracking;
            //status_running = !status_running;

            if (mode_tracking == true)
            {
                lastCommand = "TSTOP";
               // mode_tracking = false;
            }
            else
            {
                lastCommand = "TSTART";
            }

            rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " \r"));

            mode_tracking = !mode_tracking;
            updateDisplay();
        }

        private void Trackmarker_btn_Click(object sender, RoutedEventArgs e)
        {
            //mode_diagnostic = !mode_diagnostic;
            //status_running = !status_running;

            if (mode_diagnostic == true)
            {
                lastCommand = "DSTOP";
                //mode_diagnostic = false;
            }
            else
            {
                lastCommand = "DSTART";
                //mode_diagnostic = true;
            }

            rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + " \r"));

            mode_diagnostic = !mode_diagnostic;
            updateDisplay();
        }

        private void Polpwr_btn_Click(object sender, RoutedEventArgs e)
        {
            //this shouldn't be a button ! #TODO
            //status_polpwr = !status_polpwr;
            //updateDisplay();
            lastCommand = "BEEP 1";
            rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand+"\r"));
        }

        

        private void updateDisplay()
        {

            if (status_sercon == false)
            {
                status_polcon = false;
                status_polpwr = false; // actually we don't know whether power is off or on because serial comms is cut-off
            }
            
            this.Dispatcher.Invoke(() =>
            {
                if (status_sercon == false)
                {
                    connectser_img.Source = disconnect_ico;
                    connectser_box.IsEnabled = true;
                    
                }
                else
                {
                    connectser_img.Source = connect_ico;
                    connectser_box.IsEnabled = false;
                }

                if (status_polpwr == true || status_polcon == true)
                {
                    polpwr_img.Source = connect_ico;
                }
                else
                {
                    polpwr_img.Source = disconnect_ico;
                }

                polpwr_btn.IsEnabled = status_sercon;

                
                
                if ((status_init == false || status_phsr == false || status_phrq == false) && (lastCommand == "INIT" || lastCommand == "PHSR" || lastCommand == "PHRQ"))
                {
                    init_btn.IsEnabled = (status_init && status_phsr && status_phrq);
                }
                else
                {
                    init_box.IsEnabled = status_polcon;
                    init_btn.IsEnabled = status_polcon;
                }
                

                connectudp_btn.IsEnabled = status_running;
                connectudp_box.IsEnabled = status_running;


                loadtool_box.IsEnabled = status_phrq;
                loadtool_btn.IsEnabled = status_phrq;

                savetool_box.IsEnabled = status_running && mode_diagnostic;
                savetool_btn.IsEnabled = status_running && mode_diagnostic;



                trackmarker_btn.IsEnabled = !mode_tracking && status_pena;
                markerrun_btn.IsEnabled = mode_diagnostic;

                tracktool_btn.IsEnabled = !mode_diagnostic && status_pena;
                
                if(mode_diagnostic)
                {
                    trackmarker_btn.Content = "Track Individ Markers: STOP";
                }
                else
                {
                    trackmarker_btn.Content = "Track Individ Markers: START";
                }

                if (mode_tracking)
                {
                    tracktool_btn.Content = "Track Whole Tool: STOP";
                }
                else
                {
                    tracktool_btn.Content = "Track Whole Tool: START";
                }

                status_lbl.Content = currentStatusStr;

                
            });
        }

        private String getUSBSerialPortNames(String VID, String PID)
        {
            String pattern = "";
            List<String> comports = new List<string>();

            pattern = String.Format("^VID_{0}.PID_{1}", VID, PID);          
            Regex _rx = new Regex(pattern, RegexOptions.IgnoreCase);

            RegistryKey rk1 = Registry.LocalMachine;
            RegistryKey rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum");

            foreach (String s3 in rk2.GetSubKeyNames())
            {
                RegistryKey rk3 = rk2.OpenSubKey(s3);
                foreach(String s in rk3.GetSubKeyNames())
                {
                    if (_rx.Match(s).Success)
                    {
                        RegistryKey rk4 = rk3.OpenSubKey(s);
                        foreach(String s2 in rk4.GetSubKeyNames())
                        {
                            RegistryKey rk5 = rk4.OpenSubKey(s2);
                            RegistryKey rk6 = rk5.OpenSubKey("Device Parameters");
                            comports.Add((string)rk6.GetValue("PortName"));
                        }
                    }
                }
            }

            string finalVal = "ERROR: ?";

            if (comports.Count > 0)
            {
                foreach (String s in SerialPort.GetPortNames())
                {
                    if (comports.Contains(s))
                    {
                        finalVal = s;
                    }
                    else
                    {
                        finalVal =  "ERROR: Device not plugged in";
                    }
                }

            }
            else
            {
                finalVal =  "ERROR: Invalid VID/PID no";
            }

            return finalVal;
            
        }

        private void Markerrun_btn_Click(object sender, RoutedEventArgs e)
        {
            lastCommand = "3D 015";
            rs232Serial.Transmit(Encoding.ASCII.GetBytes(lastCommand + "\r"));
            //updateRichTextBox(lastCommand + "\t");

            status_running = !status_running;
            updateDisplay();
        }

        private void updateRichTextBox(string textToAppend)
        {
            //textBoxDisplay.AppendText(textToAppend);
            //Invoke(new Action(() => { textBoxDisplay.AppendText(textToAppend); }));
        }

        private void Connectser_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateDisplay();
        }

        private void Connectudp_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Init_box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


    }
}
