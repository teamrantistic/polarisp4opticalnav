﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RTSerialCom
{
    public partial class Form1 : Form
    {
        SerialClient serial1;
        int storedResponses;
        int maxLen;
        int minLen;
        int currentLen;
        string myReply = null;
        bool streamMarkerInfo = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.Items.Add("Proccessed (Bytes)");
            listBox1.Items.Add("0 Bs"); /*1*/
            listBox1.Items.Add("");
            listBox1.Items.Add("Length (Bytes)");
            listBox1.Items.Add("--------------------------");
            listBox1.Items.Add("Max");
            listBox1.Items.Add("0"); /*6*/
            listBox1.Items.Add("Min");
            listBox1.Items.Add("0"); /*8*/
            listBox1.Items.Add("Current");
            listBox1.Items.Add("0"); /*10*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            serial1 = new SerialClient("COM5", 9600);
            serial1.OnReceiving += new EventHandler<DataStreamEventArgs>(receiveHandler);
            if (!serial1.OpenConn())
            {
                MessageBox.Show(this, "The Port Cannot Be Opened", "Serial Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void receiveHandler(object sender, DataStreamEventArgs e)
        {
            storedResponses++;
            minLen = minLen > e.Response.Length ? e.Response.Length : minLen;
            maxLen = maxLen < e.Response.Length ? e.Response.Length : maxLen;
            currentLen = e.Response.Length;
            myReply += System.Text.Encoding.UTF8.GetString(e.Response, 0, currentLen);

            if (myReply != null)
            {
                if (myReply.Contains("\r"))
                {
                    Invoke(new Action(() => { richTextBox1.AppendText(myReply); richTextBox1.ScrollToCaret();}));
                    myReply = null;
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            serial1.CloseConn();
            serial1.OnReceiving -= new EventHandler<DataStreamEventArgs>(receiveHandler);
            serial1.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            listBox1.Items[1] = storedResponses.ToString();
            listBox1.Items[6] = maxLen.ToString();
            listBox1.Items[8] = minLen.ToString();
            listBox1.Items[10] = currentLen.ToString();

            

            if (streamMarkerInfo && myReply == null)
            {
                //richTextBox1.AppendText("3D 015\r");
                //serial1.Transmit(Encoding.ASCII.GetBytes("3D 015\r"));
                serial1.Transmit(Encoding.ASCII.GetBytes("TX 0801\r"));
            }




        }

        private void button3_Click(object sender, EventArgs e)
        {
            streamMarkerInfo = !streamMarkerInfo;

            //BytesTo
            //while (myReply == null)
            //{ }
            //while (storedResponses > 0)
            //{ }
            //richTextBox1.AppendText(myReply);
            //myReply = null;
            //while (serial1.BytesToRead)
            // richTextBox1.AppendText(myReply);
            //richTextBox1.AppendText("DSTART \r");
            //serial1.Transmit(Encoding.ASCII.GetBytes("DSTART \r"));
            //serial1.Transmit(Encoding.ASCII.GetBytes("3D \r"));
            //byte[] myReply = { 0, 1, 2, 3, 4, 5, 6, 7 };
            //serial1.Receive(myReply, 0, myReply.Length);
            //richTextBox1.AppendText(System.Text.Encoding.UTF8.GetString(myReply, 0, myReply.Length));
            //richTextBox1.AppendText(Convert.ToString(myReply));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                timer1.Stop();
                button4.Text = "Start";
            }
            else
            {
                timer1.Start();
                button4.Text = "Stop";
            }

        }
    }
}
